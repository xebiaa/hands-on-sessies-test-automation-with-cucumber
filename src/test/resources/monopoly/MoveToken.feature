@exercise1


Feature: Moving the token over the board

  As a player
  I want to move to new positions on the board
  So that I can get rich and win the game

  #Taking another turn
  Scenario Outline: Another turn
    Given player his turn
    When player throws <die1> and <die2>
    Then player gets another turn <anotherTurn>

  Examples:
    | die1 | die2 | anotherTurn |
    | 1    | 1    | "Yes"       |
    | 5    | 3    | "No"        |



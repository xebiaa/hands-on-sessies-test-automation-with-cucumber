package Pageobjects;

import org.junit.Assert;
import org.openqa.selenium.support.ui.LoadableComponent;
import WebDriver.SharedDriver;

/**
 * A generic page. Other pages will inherit from this object.
 */
public abstract class Page extends LoadableComponent {

    private final String relativePath;
    final SharedDriver webDriver;

    public Page(String relativePath) {
        this.webDriver = new SharedDriver();
        this.relativePath = relativePath;
    }

    /**
     * Verifies that the current page is loaded.
     */
    protected void isLoaded() {
        Assert.assertTrue(
                "The page was not loaded correctly." +
                        "\nActual URL: " + webDriver.getCurrentUrl() +
                        "\nExpected URL: " + this.relativePath

                , webDriver.getCurrentUrl().contains(relativePath)
        );
    }

    /**
     * Loads the current page.
     */
    protected void load() {
        webDriver.get("http://localhost:8080" + this.relativePath);
    }

    /**
     * Deletes all cookies in the current WebDriver session.
     */
    public void deleteCookies() {
        webDriver.deleteAllCookies();
    }

}






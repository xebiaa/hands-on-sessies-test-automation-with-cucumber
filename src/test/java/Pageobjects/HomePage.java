package Pageobjects;

import org.openqa.selenium.support.PageFactory;

public class HomePage extends Page {

    public HomePage() {
        super("/");
        PageFactory.initElements(webDriver, this);
    }


}

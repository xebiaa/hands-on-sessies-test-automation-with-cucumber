package monopoly;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import monopoly.fixture.GameFixture;
import monopoly.helpers.HelperFunctions;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class GameStepDefinitions {

    private GameFixture game;

    public GameStepDefinitions(){
        game = new GameFixture();
    }


    @Given("^player’s token is at starting position (\\d+)$")
    public void startPlayerAtPosition(int position){
        game.addPlayerAndSetPosition(position);
    }

    @When("^player throws (\\d+) and (\\d+)$")
    public void player_throws_doubles_given_value_of_dice_and_value_of_dice(int die1, int die2) throws Throwable {
        game.setDice(die1, die2);
    }

    @Then("^player’s token moves to a (\\d+)$")
    public void player_s_token_moves_to_a(int location) throws Throwable {
        game.doPlayAction();
        assertThat(game.getNewPosition(), is(location));
    }

    @Given("^player his turn$")
    public void player_his_turn() throws Throwable {
        game.addPlayerAndSetPosition(1);
    }


    @Given("^a player his turn (\\d+)$")
    public void a_player_his_turn(int turn) throws Throwable {
        game.addPlayerAndSetPosition(1);
        for(int i=1; i<turn;i++){
            game.setDice(1, 1);
            game.doPlayAction();
        }
    }


    @Then("^player gets another turn \"([^\"]*)\"$")
    public void player_gets_another_turn(String expectedResult) throws Throwable {
        game.doPlayAction();
        assertEquals(expectedResult, HelperFunctions.convertBooleanToYesOrNo(game.getPlayer().isRollAllowed()));
    }


    @Then("^player goes to jail \"([^\"]*)\"$")
    public void player_goes_to_jail(String expectedResult) throws Throwable {
        game.doPlayAction();
        assertEquals(expectedResult, HelperFunctions.convertBooleanToYesOrNo(game.getPlayer().isJailed()));
    }



}

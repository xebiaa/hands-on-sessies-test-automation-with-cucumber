package monopoly;

import pageobjects.HomePage;
import cucumber.api.java.en.Given;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by kishen on 16/09/14.
 */
public class InitPlayerDefinitions {

    @Given("^test$")
    public void test() throws Throwable {
        // Express the Regexp above with the code you wish you had
        //throw new PendingException();
        WebDriver webDriver = new FirefoxDriver();
        webDriver.get("http://localhost:8080");
        webDriver.quit();
    }
}

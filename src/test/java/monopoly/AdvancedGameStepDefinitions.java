package monopoly;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import monopoly.fixture.GameFixture;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


public class AdvancedGameStepDefinitions {


    private GameFixture game;

    public AdvancedGameStepDefinitions(){
        game = new GameFixture();
    }


    @Given("^a set of players with their starting positions$")
    public void setupGame() {
        game.setupGame();
    }

    @When("^I as player \"([^\"]*)\" start at position \"([^\"]*)\"$")
    public void I_as_player_start_at_position(String playername, String locationName) {
        game.setPlayer(playername);
        game.setLocation(locationName);
    }

    @When("^I throw the dices with value (\\d+) and (\\d+)$")
    public void I_throw_the_dices_with_value_and(int die1, int die2) throws Throwable {
        game.setDice(die1, die2);
    }

    @Then("^I should move to \"([^\"]*)\"$")
    public void I_should_move_to(String locationName) {
        game.doPlayAction();
        assertThat(game.getNewPositionName(), is(locationName));
        //do note: this does not set the isJailed... Do mark that while presenting! And show that we can enhance!
    }


    @Then("^I true should be allowed to take another turn$")
    public void I_true_should_be_allowed_to_take_another_turn() {
        game.doPlayAction();
        assertTrue(game.getPlayer().isRollAllowed());
    }

    @Then("^I false should be allowed to take another turn$")
    public void I_false_should_be_allowed_to_take_another_turn() {
        game.doPlayAction();
        assertFalse(game.getPlayer().isRollAllowed());
    }

    @Then("^I should be jailed true$")
    public void I_should_be_jailed_true() {
        assertTrue(game.getPlayer().isJailed());
    }

    @Then("^I should be jailed false$")
    public void I_should_be_jailed_false() {
        assertFalse(game.getPlayer().isJailed());
    }


}
